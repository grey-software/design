---
title: Setup
description: ""
position: 1
category: Guide
---

This package is built upon the Nuxt content docs theme.

Check the [Nuxt.js Content Docs theme documentation](https://nuxtjs.org/guides/configuration-glossary/configuration-modules) for insight on how the theme that powers this package works.

## Settings

### Custom Links

This theme provide support for the following links:

```
  "githubUrl": "https://github.com/grey-software",
  "gitlabUrl": "https://gitlab.com/grey-software",
  "discordUrl": "http://community.grey.software",
  "linkedinUrl": "https://www.linkedin.com/company/grey-software/",
  "twitter": "grey_software"
```

Head over to `content/setting.json` and set the links to your preffered endpoints.
